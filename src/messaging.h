#ifndef _BLUFF_MESSAGING_H_GUARD_
#define _BLUFF_MESSAGING_H_GUARD_

#include <pebble.h>

#define REQUEST_APPS 1
#define SEND_ID 2

void send_int(uint32_t type, uint32_t int_val);

#endif // _BLUFF_MESSAGING_H_GUARD_
